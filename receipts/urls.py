from django.urls import path
from .views import receipt_list_view, create_receipt, category_view, account_view, expensecategory_create, account_create


urlpatterns = [
    path("", receipt_list_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_view, name="category_list"),
    path("accounts/", account_view, name="account_list"),
    path("categories/create/", expensecategory_create, name="create_category"),
    path("accounts/create/", account_create, name="create_account"),
]
