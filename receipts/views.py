from django.shortcuts import render, redirect
from .models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, CreateExpenseCategoryForm, CreateAccountForm
# Create your views here.


@login_required
def receipt_list_view(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipt_list": receipt_list
    }

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_view(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "categories": categories
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_view(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def expensecategory_create(request):
    if request.method == "POST":
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            expensecategory = form.save(False)
            expensecategory.owner = request.user
            expensecategory.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategoryForm()

    context = {
        "form": form
    }

    return render(request, "categories/create.html", context)

@login_required
def account_create(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "form": form
    }

    return render(request, "accounts/create.html", context)
